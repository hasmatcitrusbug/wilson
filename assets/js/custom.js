$(document).ready(function(){ 
  var card1 = 'card1';
  
  var card3 = 'card3';
  var card4 = 'card4';
  Waves.attach('.btn', ['waves-effect']);

  /* Automotive Services */

  $(".hide-prag").addClass("display-none"); 
  
  $("#level2-cards1").addClass("display-none"); 
  $("#level2-cards2").addClass("display-none"); 
  $("#level2-cards3").addClass("display-none"); 
  $("#level3-cards2").addClass("display-none"); 

  $("#level2-vs1").addClass("display-none"); 
  $("#level2-vs2").addClass("display-none"); 
  $("#level2-vs3").addClass("display-none"); 

  $(".level2-pattern").addClass("display-none"); 

  $("#level1-btn").addClass("display-show"); 
  $("#level1-round").addClass("display-show"); 

  /* End of Automotive Services */
  
  $(".level1-btn-click").click(function(){
    
    $(".hide-prag").toggleClass("add-show-prag");  
    $(".show-prag").toggleClass("add-hide-prag"); 
    $(".text-btn-level1 ").toggleClass("add-hide-prag"); 

    $("#level1-btn").removeClass("display-show"); 
    $("#level1-round").removeClass("display-show"); 

    $(".level2-pattern").addClass("display-show"); 

    $("#level2-vs1").addClass("display-show animated fadeInLeft"); 
    $("#level2-vs2").addClass("display-show animated fadeInUp"); 
    $("#level2-vs3").addClass("display-show animated fadeInRight"); 
    
    $("#level2-vs1").click(function(){
      $("#level2-cards1").toggleClass("display-show animated fadeInUp"); 
    });

    $("#level2-vs2").click(function() {
      if (!$("#level2-cards2").hasClass("card2")) {        
        $("#level2-cards2, #level3-svg-line1").addClass("card2");
        $("#level2-cards2").removeClass("card3"); 
        $("#level2-cards2").addClass("display-show animated fadeInUp");
        $("#level2-cards2").removeClass("display-none");
        $("#level3-svg-line1").addClass("display-none");
        // $("#level3-cards2 .level3-img-card2").removeClass("add-top-height");
      } else {        
        $("#level2-cards2").removeClass("card2");        
        $("#level2-cards2").removeClass("display-show animated fadeInUp");
        $("#level2-cards2").addClass("display-none");
        $("#level3-cards2").removeClass("card2"); 
        $("#level3-cards2").addClass("display-none");
        $("#level3-cards2").removeClass("display-show");
      }


      if($("#level2-cards3").hasClass("display-show") && $("#level2-cards2").hasClass("display-none")) {
          $(".svg-icon-levels33").addClass("display-none");
          $(".svg-icon-levels33").removeClass("display-show animated fadeInUp");
      }


      $("#level2-cards2").click(function(){
        if (!$("#level3-cards2").hasClass("card2")) {        
          $("#level3-cards2").addClass("card2"); 
          $("#level3-cards2").removeClass("card3"); 
          
          $("#level3-cards2").removeClass("display-none");
          $("#level3-cards2").addClass("display-show animated fadeInUp");

          if($("#level2-cards3").hasClass("display-show")) {
            $("#level3-cards2").removeClass("card3"); 
            $("#level3-cards2").removeClass("card2"); 
            $("#level3-cards2").addClass("display-none");
            $("#level3-cards2").removeClass("display-show");
          }

        } else {
          $("#level3-cards2").removeClass("card2"); 
          $("#level3-cards2").addClass("display-none");
          $("#level3-cards2").removeClass("display-show");
        }

        if($("#level2-cards3").hasClass("display-show")) {
          $(".svg-icon-levels33").addClass("display-none");
          $(".svg-icon-levels33").removeClass("display-show animated fadeInUp");
        }

      });

    });

    $("#level2-vs3").click(function(){

      if (!$("#level2-cards3").hasClass("card3")) {
        $("#level2-cards3").addClass("card3");
        $("#level2-cards3").addClass("display-show animated fadeInUp");
        $("#level2-cards3").removeClass("display-none");
        
        $(".svg-icon-levels33").addClass("card3");
        $(".svg-icon-levels33").addClass("display-none");
        $(".svg-icon-levels33").removeClass("display-show");
        
      } else {
        $("#level2-cards3").removeClass("card3");
        $("#level2-cards3").removeClass("display-show");
        $("#level2-cards3").addClass("display-none");
      }

      $("#level2-cards3").click(function() {
        if (!$("#level3-cards2").hasClass("card3")) {        
          $("#level3-cards2").addClass("card3"); 
          $("#level3-cards2").removeClass("display-none");
          $("#level3-cards2").addClass("display-show animated fadeInUp");

          $(".svg-icon-levels33").addClass("card3");
          $(".svg-icon-levels33").removeClass("display-none");
          $(".svg-icon-levels33").addClass("display-show animated fadeInUp");
        } else {
          $("#level3-cards2").removeClass("card3"); 
          $("#level3-cards2").addClass("display-none");
          $("#level3-cards2").removeClass("display-show"); 

          $(".svg-icon-levels33").removeClass("card3");
          $(".svg-icon-levels33").addClass("display-none");
          $(".svg-icon-levels33").removeClass("display-show animated fadeInUp");
        }
      });
      // $("#level2-cards3").toggleClass("display-show animated fadeInUp"); 
      // $(".svg-icon-levels33").toggleClass("display-none"); 
      
      // $("#level2-cards3").click(function(){
      //   $("#level3-cards2").toggleClass("display-show animated fadeInUp"); 
      //   $(".svg-icon-levels33").toggleClass("display-none"); 
      //   $("#level3-svg-card2").toggleClass("display-none"); 
      //   $(".level3-img-card2").toggleClass("add-top-height display-show animated fadeInUp"); 
      // });

      // if ($("#level3-cards2").hasClass("display-show") && $("#level2-cards2").hasClass("display-show")) {
      //   $("#level2-cards3").removeClass("display-show");
      //   $("#level2-cards3").addClass("display-none");
      // } else if($("#level3-cards2").hasClass("display-show") && !$("#level2-cards2").hasClass("display-show")) {
      //   $("#level2-cards3").removeClass("display-show");
      //   $("#level2-cards3").addClass("display-none");

      //   $("#level3-cards2").removeClass("display-show");
      //   $("#level3-cards2").addClass("display-none");
        
      // } else {
      //   $("#level2-cards3").addClass("display-show");
      //   $("#level2-cards3").removeClass("display-none");
      // }

    });


  });

  /* Insurance Services */

  $("#level1-btn-s2").addClass("display-show"); 
  $("#level1-round-s2").addClass("display-show"); 
  $(".level2-pattern-s2").addClass("display-none"); 
  
  $("#level2-cards1-s2").addClass("display-none"); 
  $("#level2-vs1-s2").addClass("display-none"); 
 
  $(".level1-btn-click-s2").click(function(){
    
    $(".hide-prag").toggleClass("add-show-prag");  
    $(".show-prag").toggleClass("add-hide-prag"); 
    $(".text-btn-level1-s2").toggleClass("add-hide-prag"); 

    $("#level1-round-s2").removeClass("display-show"); 


    $("#level1-btn-s2").removeClass("display-show"); 
    $("#level2-vs1-s2").addClass("display-show animated fadeInUp"); 
    
    $("#level2-vs1-s2").click(function(){
      $("#level2-cards1-s2").toggleClass("display-show animated fadeInUp"); 
    });

  });

  /* End of Insurance Services */

 });

  
